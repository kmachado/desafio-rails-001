# Criar uma aplicação Ruby on Rails de consulta na api do [Dribbble](https://dribbble.com)#

Criar uma aplicação Rails para consultar a [Dribbble API](http://developer.dribbble.com/v1/) e trazer os shots + populares . Basear-se no mockup fornecido:

![Screen Shot 2014-10-09 at 3.42.06 PM.png](https://bitbucket.org/repo/bApLBb/images/3039998141-Screen%20Shot%202014-10-09%20at%203.42.06%20PM.png)

### **Deve conter** ###

* Implementado com responsividade web(Sugestão Bootstrap).
* Lista de shots. Exemplo de chamada na API: http://api.dribbble.com/shots/popular?page=1
* Paginação na tela de lista (aumentando o parâmetro "page").
* Detalhe de um shot. Exemplo de chamada na API: http://api.dribbble.com/shots/1757954
* Tela de detalhe de um shot deve conter autor com foto.

### **Ganha + pontos se acrescentar** ###

* Carregamento assíncrono das imagens e sob demanda
* Sistema de build e gestão de dependências no projeto.
* Testes no projeto (unitários e por tela). 
* Testes funcionais (que naveguem pela aplicação como casos de uso). 
* Cache de imagens e da API. 


### **OBS** ###

A foto do mockup é meramente ilustrativa, não existe necessidade de fazer share em redes sociais. 
Dito isso, consideraremos como um plus caso seja feito.  

Inspire-se nas telas(mockup de uma app nativa)  para implementar a responsividade. Pode usar o Twitter Bootstrap se quiser.

### **Processo de submissão** ###
O candidato deverá implementar a solução e enviar por email a URL do repositório público Git (Github ou Bitbucket).

